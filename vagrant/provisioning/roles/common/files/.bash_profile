alias l="ls -lhaF"
alias ls="ls --color=tty"
alias u='cd ..'
alias uu='cd ../..'
alias uuu='cd ../../..'
alias uuuu='cd ../../../..'

# Enable bash history
# http://www.davidcramer.net/code/129/bash-trick-history-searching-ala-ipython.html
export HISTCONTROL=erasedups
export HISTSIZE=10000
shopt -s histappend
# These set up/down to do the history searching
bind '"\e[A"':history-search-backward
bind '"\e[B"':history-search-forward
